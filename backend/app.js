// Importa los paquetes
const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");
const tareasRouter = require("./routes/tareas");
require("dotenv").config();

// Crea la aplicación
const app = express();

// Middleware
app.use(cors());
app.use(express.json()); // Permite procesar el body de las peticiones
app.use("/tareas", tareasRouter);

// Conexión a la base de datos
mongoose.connect(
  process.env.DB_CONNECTION_ATLAS,
  {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  },
  () => {
    console.log("Conectado a la base de datos...");
  }
);

// Inicia el servidor
app.listen(process.env.PORT, () => {
  console.log(`Servidor en ejecución por el puerto ${process.env.PORT}...`);
});
