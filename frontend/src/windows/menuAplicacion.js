const { Menu } = require("electron");
const EventEmitter = require("./Emitter.js");
const myEmitter = new EventEmitter();

const crearMenuAplicacion = (app) => {
  // Menu
  const templateMenu = [
    {
      label: "Tareas",
      submenu: [
        {
          label: "Nueva tarea",
          accelerator: "Ctrl+N",
          click() {
            // Envia el evento a ipcMain para que muestre la ventana de crear tarea
            // ipcRenderer.send("mostrar-ventana-crear-tarea");
            myEmitter.emit("eventOne");
            console.log("Emitió el eventOne");
          },
        },
        {
          label: "Salir",
          accelerator: process.platform === "darwin" ? "command+Q" : "Ctrl+Q",
          click() {
            // Envia el evento a ipcMain para que cierre la aplicación
            // ipcRenderer.send("cerrar-aplicacion");
            app.quit();
          },
        },
      ],
    },
  ];

  // Verifica si el sistema operativo es Mac os
  if (process.platform === "darwin") {
    // Elimina del menú la opción con el nombre de la app
    templateMenu.unshift({
      label: app.getName(),
      // label: "Tareas",
    });
  }

  // Verifica que la aplicación no esté empaquetada y que
  // esté en ambiente de desarrollo
  if (!app.isPackaged) {
    templateMenu.push({
      label: "DevTools",
      submenu: [
        {
          label: "Mostrar/Ocultar DevTools",
          click(item, focusedWindow) {
            focusedWindow.toggleDevTools();
          },
        },
        {
          role: "reload",
        },
      ],
    });
  }

  // Crea el menú
  const menuAplicacion = Menu.buildFromTemplate(templateMenu);

  // Asigna el menu a la aplicación
  Menu.setApplicationMenu(menuAplicacion);
};

module.exports = crearMenuAplicacion;
