# Repositorio de Aplicaciones de Software para Telcomunicaciones 20211

_Profesor:_ John Cardozo

## Descripción

este proyecto es para ejemplificar el desarrollo de aplicaciones con comunicación con Rest API

## Herramientas

- VSCode
- Git

## Lenguajes

- Python
- HTML
- CSS
- Javascript

## Tecnologías

- NodeJS
- Electron
- MongoDB
