const mongoose = require("mongoose");

// Definición del esquma
const TareaSchema = mongoose.Schema({
  nombre: {
    type: String,
    required: true,
  },
  terminada: {
    type: Boolean,
    default: false,
  },
});

module.exports = mongoose.model("Tarea", TareaSchema);
