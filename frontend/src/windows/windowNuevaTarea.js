const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

class WindowNuevaTarea extends BrowserWindow {
  constructor() {
    super({
      width: 500,
      height: 300,
      title: "Nueva tarea",
      // Habilita la ventana para acceder a librerías de Nodejs
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false,
      },
    });

    // Carga el template HTML
    this.loadURL(
      url.format({
        pathname: path.join(__dirname, "../templates/nuevaTarea.html"),
        protocol: "file",
        slashes: true,
      })
    );

    // Elimina el menú de la ventana (Windows & Linux)
    this.setMenu(null);

    // Escucha el evento "closed" para limpiar memoria
    this.on("closed", () => {
      //this = null;
    });
  }
}

module.exports = WindowNuevaTarea;
