async function obtenerTareas() {
  try {
    // Obtiene las tareas del backend
    let tareas = await axios.get("http://localhost:3000/tareas");

    // Obtiene el <ul>
    let ulTareas = document.getElementById("listaTareas");
    ulTareas.innerHTML = "";

    // Recorre las tareas para crear <li>
    tareas.data.forEach((tarea) => {
      // Crea un <li>
      let li = document.createElement("li");
      li.innerText = tarea.nombre;

      // Agrega el <li> al <ul>
      ulTareas.appendChild(li);
    });
  } catch (error) {
    console.log(error);
  }
}

async function crearTarea() {
  const nombreTarea = document.getElementById("nombreTarea");
  const texto = nombreTarea.value;

  try {
    // Crea la tarea en el backend
    await axios.post("http://localhost:3000/tareas", {
      nombre: texto,
    });

    // Obtiene las tareas del backend
    obtenerTareas();

    // Limpia el campo de texto
    nombreTarea.value = "";
  } catch (error) {
    console.log(error);
  }
}

const boton = document.getElementById("botonCrearTarea");

boton.addEventListener("click", crearTarea);

obtenerTareas();
