document.addEventListener("DOMContentLoaded", () => {
  // Obtiene el ipcRenderer
  const { ipcRenderer } = require("electron");

  // Escucha el evento "nueva-tarea"
  ipcRenderer.on("nueva-tarea", (evento, datos) => {
    // Obtiene el <ul>
    const ul = document.getElementById("listaTareas");

    // Crea un <li>
    const li = document.createElement("li");
    li.textContent = datos.tarea;

    // Agrega el <li> al <ul>
    ul.appendChild(li);
  });

  // Obtiene el botón para crear una tarea
  const botonNuevaTarea = document.getElementById("botonCrearTarea");
  botonNuevaTarea.addEventListener("click", () => {
    // Envia el evento a ipcMain para que muestre la ventana de crear tarea
    ipcRenderer.send("mostrar-ventana-crear-tarea");
  });
});
