// Librerías
const { app, Menu } = require("electron");
const path = require("path");
const { ipcMain } = require("electron");

// Importa las ventanas
const WindowPrincipal = require("./windows/windowPrincipal.js");
const WindowNuevaTarea = require("./windows/windowNuevaTarea.js");

// Ventanas de la aplicación
let windowPrincipal;
let windowNuevaTarea;

// La aplicación escucha el evento "ready"
app.on("ready", () => {
  // Crea la ventana principal
  windowPrincipal = new WindowPrincipal(app);

  // Crea el menu de la aplicación
  crearMenuAplicacion();

  // Escucha el evento "nueva-tarea"
  ipcMain.on("nueva-tarea", (evento, datos) => {
    // Envía los datos a la ventana principal
    windowPrincipal.webContents.send("nueva-tarea", datos);

    // Cierra la ventana de nueva tarea
    windowNuevaTarea.close();
  });

  ipcMain.on("mostrar-ventana-crear-tarea", (evento, datos) => {
    // Crea la ventana para crear una nueva tarea
    windowNuevaTarea = new WindowNuevaTarea();
  });
});

// TODO: Extraer el menu a un archivo aparte
// Esto se debe hacer con eventEmitters:
// https://www.ictshore.com/fullstack-course/javascript-node-js-events-in-multiple-files/
// https://stackoverflow.com/questions/13466227/nodejs-singleton-events/31408123

const crearMenuAplicacion = () => {
  // Crea el menú
  const menuAplicacion = Menu.buildFromTemplate(templateMenu);
  // Asigna el menu a la aplicación
  Menu.setApplicationMenu(menuAplicacion);
};

// Menu
const templateMenu = [
  {
    label: "Tareas",
    submenu: [
      {
        label: "Nueva tarea",
        accelerator: "Ctrl+N",
        click() {
          // Crea la ventana para crear una nueva tarea
          windowNuevaTarea = new WindowNuevaTarea();
        },
      },
      {
        label: "Salir",
        accelerator: process.platform === "darwin" ? "command+Q" : "Ctrl+Q",
        click() {
          // Cierra la aplicación
          app.quit();
        },
      },
    ],
  },
];

// Verifica si el sistema operativo es Mac os
if (process.platform === "darwin") {
  // Elimina del menú la opción con el nombre de la app
  templateMenu.unshift({
    label: app.getName(),
  });
}

// Verifica que la aplicación no esté empaquetada
if (!app.isPackaged) {
  templateMenu.push({
    label: "DevTools",
    submenu: [
      {
        label: "Mostrar/Ocultar DevTools",
        click(item, focusedWindow) {
          focusedWindow.toggleDevTools();
        },
      },
      {
        role: "reload",
      },
    ],
  });
}

// Carga las librerías de desarrollo solo
// cuando la aplicación no está empaquetada
if (!app.isPackaged) {
  try {
    // Revisa los archivos HTML
    require("electron-reload")(__dirname, {
      electron: path.join(__dirname, "node_modules", ".bin", "electron"),
      hardResetMethod: "exit",
    });
    // Revisa los archivos Javascript
    require("electron-reloader")(module, {
      debug: true,
      watchRenderer: true,
    });
  } catch (error) {
    console.error(error);
  }
}
