# Instrucciones de MongoDB

## Crear un colección

```javascript
db.createCollection("peliculas");
```

## Eliminar una colección

```javascript
db.directores.drop();
```

## Crear un nuevo documento

```javascript
db.peliculas.insert({
  titulo: "Pulp Fiction",
  anio: 1994,
});
```

## Insertar obteniendo el objeto guardado

```javascript
db.peliculas.insert(
  {
    titulo: "Kill Bill Vol 1",
    anio: 2003,
  },
  function (err, documento) {
    console.log(documento);
  }
);
```

## Insertar varios documentos a la vez

```javascript
db.peliculas.insert([
  { titulo: "Django Unchained", anio: 2012 },
  { titulo: "Once upon a time in Hollywood", anio: 2019 },
]);
```

## Inserta un documento con un sub-documento

```javascript
db.peliculas.insert({
  titulo: "Inglorious Basterds",
  anio: 2009,
  director: {
    nombres: "Quentin",
    apellidos: "Tarantino",
  },
});

db.peliculas.insert({
  titulo: "The Hateful Eight",
  anio: 2015,
  director: {
    nombres: "Quentin",
    apellidos: "Tarantino",
  },
  actores: ["Samuel", "Kurt", "Tim"],
});
```

## Obtiene todos los documentos de una colección

```javascript
db.peliculas.find();
```

## Obtiene documentos dado un criterio

```javascript
db.peliculas.find({
  anio: 2003,
});
```

## Obtener documentos por \_id

```javascript
db.peliculas.find({
  _id: ObjectId("60a438fa893557189edc38f3"),
});

db.peliculas.find({
  anio: { $lte: 2003 },
});
```

## Obtener documentos con critoerio OR

```javascript
db.peliculas.find({
  $or: [{ anio: 1994 }, { anio: 2003 }],
});
```

## Obtener documentos con critoerio AND

```javascript
db.peliculas.find({
  $and: [{ anio: { $lt: 2015 } }, { anio: { $gt: 2000 } }],
});
```

## Obtiene los documentos que cunplan con una codición de un sub-documento

```javascript
db.peliculas.find({
  "director.nombres": "Quentin",
});
```

## Obtiene los documentos que tengan un valor dentro de un arreglo

```javascript
db.peliculas.find({
  actores: "Samuel",
});
```

## Modifica TODO el documento

```javascript
db.peliculas.update({ titulo: "Pulp Fiction" }, { anio: 1993 });
```

## Modifica una parte del documento

```javascript
db.peliculas.update(
    { \_id: ObjectId("60a43589893557189edc38f1") },
    {
        $set: {
            titulo: 'Pulp Fiction'
        }
    }
);
```

## UPSERT: si no lo encuentra: lo crea. Si lo encuentra: lo actualiza

```javascript
db.peliculas.update(
  { titulo: "Titanic" },
  { titulo: "The Notebook", anio: 2002 },
  { upsert: true }
);
```

## Obtiene los documentos que tengon un atributo

```javascript
db.peliculas.find({ director: { $exists: true } });
```

## UNSET elimina un atributo del documento

```javascript
db.peliculas.update(
  { _id: ObjectId("60a443ad893557189edc38f6") },
  { $unset: { actores: 1 } }
);
```

## Renombre un atributo de la colección

```javascript
db.peliculas.update({ titulo: "The Notebook" }, { $rename: { year: "anio" } });
```

## Elimina un atributo a todos los documentos que cumplen con un criterio

```javascript
db.peliculas.updateMany(
  { director: { $exists: true } },
  { $unset: { director: true } }
);
```

## Modifica varios documentos

```javascript
db.peliculas.updateMany({ anio: 2015 }, { $set: { titulo: "ABC" } });
```

## Elimina todos los documentos que cumplan con el criterio

```javascript
db.peliculas.remove({ titulo: "ABC" });
```

## Iteración sobre documentos

```javascript
db.peliculas.find().forEach(function (documento) {
  print("El titulo de la pelicula es: " + documento.titulo);
});
```

## Creación de usuarios

```javascript
db.createUser({
  user: "john",
  pwd: "123",
  roles: ["readWrite", "dbAdmin"],
});
```
