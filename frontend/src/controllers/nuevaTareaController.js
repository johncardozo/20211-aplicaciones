document.addEventListener("DOMContentLoaded", () => {
  // Importa el objeto ipcRenderer
  const { ipcRenderer } = require("electron");

  // Obtiene los elementos de la ventana
  const boton = document.getElementById("botonCrearTarea");
  const tareaText = document.getElementById("tareaText");

  boton.addEventListener("click", (evento) => {
    // Previene el comportamiento por defecto
    evento.preventDefault();
    // Obtiene el texto digitado por el usuario
    const texto = tareaText.value;
    // Crea el objeto de datos
    const datos = {
      tarea: texto,
    };
    // Envía el evento a quien esté escuchando
    ipcRenderer.send("nueva-tarea", datos);
  });
});
