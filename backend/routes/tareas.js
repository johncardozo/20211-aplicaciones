const express = require("express");
const Tarea = require("../models/tarea");

// Crea el router
const router = express.Router();

// Rutas
router.get("/:id", async (req, res) => {
  // Obtiene el valor del id
  const id = req.params.id;

  try {
    // Obtiene la tarea por el Id
    const tarea = await Tarea.findById(id);
    // Verifica si se econtró una tarea en la BD
    if (tarea !== null) {
      return res.json(tarea);
    } else {
      return res.status(404).send({ mensaje: "No se encontró la tarea" });
    }
  } catch (error) {
    // Retorna el error al cliente
    return res.status(500).send({ mensaje: "No se pudo crear la tarea" });
  }
});

router.get("/", async (req, res) => {
  try {
    // Obtiene todas las tareas
    const tareas = await Tarea.find();

    return res.json(tareas);
  } catch (error) {
    // Retorna el error al cliente
    return res.status(500).send({ mensaje: "No se pudo obtener la tarea" });
  }
});

router.post("/", async (req, res) => {
  // Obtiene el body de la petición
  const body = req.body;

  try {
    // Crea el objeto
    const tarea = new Tarea({
      nombre: body.nombre,
      terminada: body.terminada,
    });

    // Guarda el objeto en BD
    const resultado = await tarea.save();

    // Retorna el resultado
    return res.status(200).send(resultado);
  } catch (error) {
    // Retorna el error al cliente
    return res.status(500).send({ mensaje: "No se pudo crear la tarea" });
  }
});

router.patch("/:id", async (req, res) => {
  // Obtiene el parametro id y el body
  const id = req.params.id;
  const body = req.body;

  try {
    // Actualiza la tarea en la BD
    const resultado = await Tarea.findOneAndUpdate({ _id: id }, body, {
      new: true,
    });

    // Retorna el resultado al cliente
    return res.json(resultado);
  } catch (error) {
    // Retorna el error al cliente
    return res.status(500).send({ mensaje: "No se pudo editar la tarea" });
  }
});

router.delete("/:id", async (req, res) => {
  // Obtiene el id
  const id = req.params.id;
  try {
    // Elimina la tarea
    await Tarea.deleteOne({ _id: id });

    // Retorna
    return res.json({ mensaje: "Se eliminó la tarea" });
  } catch (error) {
    // Retorna el error al cliente
    return res.status(500).send({ mensaje: "No se pudo eliminar la tarea" });
  }
});

module.exports = router;
