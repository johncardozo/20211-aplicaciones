const { BrowserWindow } = require("electron");
const url = require("url");
const path = require("path");

class WindowPrincipal extends BrowserWindow {
  constructor() {
    super({
      // Habilita la ventana para acceder a librerías de Nodejs
      webPreferences: {
        nodeIntegration: true,
        contextIsolation: false,
      },
    });
    // Carga el template HTML
    this.loadURL(
      url.format({
        pathname: path.join(__dirname, "../templates/principal.html"),
        protocol: "file",
        slashes: true,
      })
    );
    // Escucha el evento closed para cerrar la aplicación
    this.on("closed", () => {
      // Cierra la aplicación
      app.quit();
    });
  }
}

module.exports = WindowPrincipal;
